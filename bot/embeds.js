import Discord from 'discord.js';

// Shopping list embeds
/**
 * Convenience function for generating a shopping list embed
 * @param {string} title Shopping list title
 * @param {string} items stringified (+ newlines) shopping list
 * @returns {Discord.MessageEmbed}
 */
export const shoppingListEmbed = (title, items) => {
  return new Discord.MessageEmbed()
    .setColor('#0099ff')
    .setTitle(title)
    .setDescription('Your shopping list')
    .addField('Items on your list', `\`\`\`${items}\`\`\``);
};
