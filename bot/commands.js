// eslint-disable-next-line no-unused-vars
import Discord from 'discord.js';
import { shoppingHandler } from './shopping.js';
import { reminderHandler } from './reminders.js';

const commands = {
  /**
   * Handle help command
   * @param {string} command subcommand to operate on
   * @param {Discord.Message} message discord.js message object
   */
  '!help': (command, message) => {
    console.log('Sending help');
    message.reply('I am here 2 help');
  },
  '!sl': shoppingHandler,
  /**
   * Handle reminder command
   * @param {string} command subcommand to operate on
   * @param {Discord.Message} message discord.js message object
   */
  '!remindme': (command, message) => {
    reminderHandler(message);
  },
};
const commandStrings = Object.keys(commands);

/**
 * ## subCommandParser
 * @param {string} message Message string passed from message listener
 *
 * ### About
 * Gets the subcommand from a string like this:
 *
 * `!sl create List Name`
 *
 * This can technically live at the feature file level,
 * but all commands are going to use this, so we don't want to duplicate
 */
export const subCommandParser = (message) => {
  const subCommandRegex = /!\w+ (\w+)/g;
  return subCommandRegex.exec(message);
};

/**
 * @param {Discord.Message} message Message object passed from message listener
 */
export const commandParser = (message) => {
  if (message.content.startsWith('!')) {
    // Loop through possible commands and execute/break on matching one
    for (let i = 0; i < commandStrings.length; i++) {
      // Only worry about matching commands
      if (message.content.includes(commandStrings[i])) {
        // Command matches, execute it then break out of loop
        const match = commandStrings[i];

        // ? Check for null subcommand when using just a base command like `!sl`
        const parsed = subCommandParser(message.content);
        const subCommand = parsed ? parsed[1] : null;
        commands[match](subCommand, message);
        break;
      }
    }
  }
};
