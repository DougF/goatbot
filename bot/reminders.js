// eslint-disable-next-line no-unused-vars
import Discord from 'discord.js';
import { Logger } from '../logger/log.js';
import { ReminderActions, useGoatBot } from './core.js';

/**
 *
 * @param {Discord.Message} message
 */

// eslint-disable-next-line no-unused-vars
export const reminderHandler = async (message) => {
  await message.channel.startTyping();
  try {
    const { bot } = await useGoatBot();
    const logger = new Logger('reminders.js', 'reminder');
    logger.info('running reminder handler');

    const contents = message.content
      .replace('!remindme', '') // remove the parent command
      .split('|') // Get message, reminder time, recurring
      .map(s => s.trim()); // Remove extra whitespace for each element

    if (contents.length && contents[0]) {

      // Put reminder logic here
      await bot.agenda.schedule(`in ${contents[1] || '1 hour'}`, ReminderActions.RemindUser, {
        reminder: contents[0],
        messageSender: message.author.id,
      });

      message.channel.stopTyping();
      await message.channel.send(`Got it, I'll send you "${contents[0]}" in ${contents[1] || '1 hour'}`);
      return;
    }

    await message.reply('Command `!remindme` must have reminder content! See `!help` for more details');
  }catch(e) {
    console.error(e);
    message.channel.stopTyping();
    await message.channel.send('Something went wrong, please check the logs');
  }
};