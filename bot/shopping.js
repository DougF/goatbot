import { getUnixTime } from 'date-fns';
import Discord from 'discord.js';

import { initDB } from '../data/firestore.js';
import { Logger } from '../logger/log.js';
import { shoppingListEmbed } from './embeds.js';

const log = new Logger('shopping.js', 'sl-command');

const ShoppingPaths = {
  base: 'shopping-lists',
  items: 'items',
};

const ExpectedShoppingErrors = {
  ShoppingListEmpty: 'ShoppingListEmpty',
};

/**
 *
 * @param {string} userID
 * @returns {Promise<{id: string, title: string, ownedBy: userID, created: number, updated: number, active: true}>}
 */
const getActiveList = async (userID) => {
  const db = initDB();
  // Get current active list
  const activeList = await db
    .collection(ShoppingPaths.base)
    .where('ownedBy', '==', userID)
    .where('active', '==', true)
    .limit(1)
    .get();

  // If no active list, error out
  if (activeList.empty) {
    throw new Error(ExpectedShoppingErrors.ShoppingListEmpty);
  }

  // You must enumerate the query list, so no list[0] convenience here
  let selectedList = null;
  activeList.forEach((doc) => {
    selectedList = {
      id: doc.id,
      ...doc.data(),
    };
  });

  return selectedList;
};

const slCommands = {
  /**
   * Add items to a subcollection on a list
   * @param {string} args - This string is the list title to create
   * @param {Discord.Message} message
   */
  add: async (args, message) => {
    const userID = message.member.id,
      db = initDB();

    // Split a csv + potential spaced list into an array of trimmed strings
    const items = args.split(',').map((val) => val.trim());

    try {
      // Get current active list
      const activeList = await getActiveList(userID);

      // Finally create the batch updater
      const batch = db.batch();
      items.forEach((item) => {
        // Creates a firestore document reference that looks like:
        // `shopping-list/list-id/items/item
        const itemDocReference = db
          .collection(ShoppingPaths.base)
          .doc(activeList.id)
          .collection(ShoppingPaths.items)
          .doc();

        batch.create(itemDocReference, {
          item,
          quantity: 1, // TODO: Add support for setting this number
          complete: false,
        });
      });

      await batch.commit();

      message.channel.send(`Added items to ${activeList.title}: \`${args}\``);
    } catch (e) {
      console.error(e);
    }
  },
  /**
   * Handle creating lists
   * @param {string} args - This string is the list title to create
   * @param {Discord.Message} message
   */
  create: async (args, message) => {
    const title = args,
      userID = message.member.id,
      db = initDB();

    // TODO: Try and put this in a transaction instead of multiple queries
    //? Should yield only one as only once can be active at a time

    const batch = db.batch();

    // Mark current active list as inactive as part of write batch
    const activeShoppingLists = await getActiveList(userID);
    batch.update(
      db.collection(ShoppingPaths.base).doc(activeShoppingLists.id),
      {
        active: false,
        updated: getUnixTime(new Date()),
      },
    );

    const document = db.collection(`shopping-lists`).doc();
    batch.create(document, {
      title,
      ownedBy: userID,
      created: getUnixTime(new Date()),
      updated: getUnixTime(new Date()),
      active: true,
    });

    // This batch will have some combination of updates for active lists, and one create for the new list
    await batch.commit();
    message.channel.send(`Created and activated new list - ${title}`);
  },

  /**
   * Sends a formatted object for a requested list
   * @param {string} args - This string is the list title to create
   * @param {Discord.Message} message
   */
  debug: async (args, message) => {
    const title = args,
      userID = message.member.id,
      db = initDB();

    const queryRes = await db
      .collection(ShoppingPaths.base)
      .where('ownedBy', '==', userID)
      .where('title', '==', title)
      .limit(1)
      .get();

    const docArray = [];
    queryRes.forEach((doc) => {
      docArray.push(doc);
    });

    const doc = docArray[0]?.data() || {};
    message.channel.send(`\`${JSON.stringify(doc, null, 2)}\``);
  },

  /**
   * Handle creating lists
   * @param {string} args - This string is the list title to create
   * @param {Discord.Message} message
   */
  help: (args, message) => {
    message.reply(
      `
How to use \`!sl\`
\`\`\`
!sl help - Shows this message
---
!sl create <your list> - Creates and selects a new shopping list with given name
---
!sl select <your list> - Sets selected list as active (CASE and SPACE SENSITIVE)
---
\`\`\`
`,
    );
  },

  /**
   * Handle creating lists
   * @param {string} args - This string is the list title to create
   * @param {Discord.Message} message
   */
  show: async (args, message) => {
    const userID = message.member.id,
      db = initDB();

    /**
     * Format shopping list in a fancy way
     * @param {string} title Title of list you're displaying
     * @param {string | Array<{ item: string, quantity: number, complete: boolean }>} items Formated string of items
     * @returns {string}
     */
    const formatShoppingList = (title, items) => {
      const formattedItems = Array.isArray(items)
        ? items.map((item) => `${item.item}: ${item.quantity}`).join('\n')
        : items;
      return shoppingListEmbed(title, formattedItems);
    };

    try {
      const activeList = await getActiveList(userID);
      const collection = await db
        .collection(ShoppingPaths.base)
        .doc(activeList.id)
        .collection(ShoppingPaths.items)
        .get();

      // Won't be empty often so just show a simple message
      if (collection.empty) {
        message.channel.send(formatShoppingList(activeList.title, 'No items'));
      } else {
        // If there are items in the shopping list, pull the data and format
        /** @type {Array<{ item: string, quantity: number, complete: boolean }>} */
        const items = [];
        collection.forEach((doc) => {
          items.push(doc.data());
        });
        message.channel.send(formatShoppingList(activeList.title, items));
      }
    } catch (e) {
      console.error(e);
      message.channel.send(
        `[ERROR] \`${e.name || 'Unknown Error Type'}: ${e.message}\``,
      );
    }
  },
};

const slCommandList = Object.keys(slCommands);

/**
 * The uppermost handler/parser for !shopping commands
 * @param {string | null} command Extracted subcommand from message content
 * @param {Discord.Message} message Message object passed from bot's 'on message' handler
 */
export const shoppingHandler = (command, message) => {
  log.info(`Handling '${command}' request`);
  slCommandList.forEach((key) => {
    if (key !== command) return;
    const messageArgs = message.content.split(command).pop().trim();
    slCommands[key](messageArgs, message);
  });
};
