// require the discord.js module
import Discord from 'discord.js';
import { Logger } from '../logger/log.js';
import { commandParser } from './commands.js';
import { Agenda } from 'agenda';

export const ReminderActions = {
  RemindUser: 'RemindUser',
};

export class GoatBot {
  /**
   * Constructor for an instance of GoatBot
   * @param {object} config The actual config
   * @param {string} config.BOT_KEY String token gathered from .env
   */
  constructor(config) {
    this.BOT_KEY = config.BOT_KEY;
    this.initialized = false;
    this.agenda = null;
    this.client = new Discord.Client();
    this.log = new Logger('GoatBot', 'bot-message');
  }

  async startAgenda() {
    const logger = new Logger('core.js', 'agenda');
    // eslint-disable-next-line no-async-promise-executor
    if (!process.env.MONGO_CONNECTION_STRING) {
      throw new Error('[Mongo Error] Cannot find connection string in ENV');
    }

    const agenda = this.agenda || new Agenda({
      db: {
        address: process.env.MONGO_CONNECTION_STRING,
        options: {
          useNewUrlParser: true,
          useUnifiedTopology: true,
        },
      },
    });
    await agenda.start();
    if (!this.agenda) {
      agenda.define(ReminderActions.RemindUser, async (job) => {
        const { reminder, messageSender } = job.toJSON().data;
        await this.sendDirectMessage(messageSender, `Hiya! Here's your reminder: "${reminder}"`);
        logger.info(`Sent DM reminder to ${messageSender}`);
      });
    }
    this.agenda = agenda;
  }

  async startBot() {
    this.initialized = true;

    // ? Display simple started message for debug purposes
    this.client.once('ready', () => {
      this.log.info('GoatBot is started and ready for commands');
    });

    this.client.on('message', commandParser);

    // TODO: Wrap in try/catch
    return typeof (await this.client.login(this.BOT_KEY)) === 'string';
  }

  stopBot() {
    this.client.destroy();
  }

  // Delayed/nonstandard messages
  /**
   *
   * @param {string} member
   * @param {string} content
   */
  async sendDirectMessage(member, content) {
    const dm = await this.client.users.resolve(member).createDM();
    dm.send(content);
  }
}

let gb;
export const useGoatBot = async () => {
  const bot = gb || new GoatBot({
    BOT_KEY: process.env.BOT_TOKEN,
  });
  if (!gb) {
    await bot.startBot();
    await bot.startAgenda();
  }

  gb = bot;
  return { bot: gb };
};

