export class Logger {
  constructor(fileName, label) {
    this.fileName = fileName;
    this.label = label;
  }

  /**
   * Takes in the message, outputs a string
   * @param {string|object|array} message The message(s) to format
   * @returns {string}
   */
  formatMessage(message) {
    if (Array.isArray(message)) {
      return message.join(' ');
    } else if (typeof message === 'object') {
      return JSON.stringify(message);
    }
    return message;
  }

  /**
   *
   * @param {string|object|array} message
   */
  info(message) {
    console.info(`[${this.label}] ${this.formatMessage(message)}`);
  }

  /**
   *
   * @param {string|object|array} message
   */
  error(message) {
    console.error(`[${this.label}] ${this.formatMessage(message)}`);
  }

  /**
   *
   * @param {string|object|array} message
   */
  debug(message) {
    if (process.env.NODE_ENV === 'development') {
      console.log(`[DEBUG ${this.label}] ${this.formatMessage(message)}`);
    }
  }
}
