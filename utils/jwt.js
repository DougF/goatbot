import jwt from 'jsonwebtoken';

/**
 * Encode some data using the private key
 * @param {Record<string, unknown>} data usually a user object, but any object to encode
 * @returns {string | null}
 */
export const encode = async (data) => {
  // Valid for two weeks
  try {
    // TODO: Consider changing this expiry to match discord token
    const token = jwt.sign(data, process.env.JWT_SECRET, { expiresIn: '14d' });
    return token;
  } catch (e) {
    console.error(e);
    return null;
  }
};

/**
 * Attempt to verify token using private key
 * @param {Record<string, unknown>} data usually a user object, but any object to encode
 * @returns {string | null}
 */
export const decode = async (data) => {
  // Valid for two weeks
  try {
    const token = !!jwt.verify(data, process.env.JWT_SECRET);
    return token;
  } catch (e) {
    console.error(e);
    return null;
  }
};
