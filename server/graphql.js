import { ApolloError, ApolloServer, gql } from 'apollo-server-express';
import axios from 'axios';
import qs from 'qs';
import { Logger } from '../logger/log.js';
import { encode } from '../utils/jwt.js';

const queryLog = new Logger('graphql.js', 'gql-query');

// TODO: Add schema merging here
export const schema = gql`
  type Query {
    hello_world: String!
    getDiscordInformation(token: String!): DiscordUser!
  }

  type Mutation {
    register_user(token: String!): User!
    login(token: String!): User!
  }

  type DiscordUser {
    id: String!
    username: String!
    avatar: String!
    discriminator: String!
    public_flags: Int!
    flags: Int!
    locale: String!
    mfa_enabled: Boolean!
    premium_type: Int
    email: String!
    verified: Boolean!
  }

  input DiscordUserInput {
    id: String!
    username: String!
    avatar: String!
    discriminator: String!
    public_flags: Int!
    flags: Int!
    locale: String!
    mfa_enabled: Boolean!
    premium_type: Int
    email: String!
    verified: Boolean!
  }

  input UserRegistrationInput {
    discord: DiscordUserInput
    password: String!
  }

  type User {
    # Internal ID to use
    id: String!
    password: String
    # External user data
    discord: DiscordUser
    # Token only returned on login/register
    token: String
  }
`;

export const resolvers = {
  Query: {
    hello_world: () => {
      queryLog.info('received hello world query');
      return 'Hello, GQL!';
    },
    /**
     *
     * @param {unknown} _
     * @param {{ token: string }} data
     * @returns {Record<string, any>}
     */
    async getDiscordInformation(_, data) {
      const code = data.token;
      const url = 'https://discord.com/api/oauth2/token';

      // Format discord-required application data
      const body = qs.stringify({
        client_id: process.env.DISCORD_CLIENT_ID,
        client_secret: process.env.DISCORD_CLIENT_SECRET,
        grant_type: 'authorization_code',
        code,
        redirect_uri: process.env.DISCORD_REDIRECT,
        scope: 'identify email guilds connections',
      });

      try {
        const res = await axios.post(url, body, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        });
        /** @type {{ access_token: string, expires_in: number, refresh_token: string, scope: string, token_type: string,}} */
        const data = res.data;
        const sharedConfig = {
          headers: {
            Authorization: `${data.token_type} ${data.access_token}`,
          },
        };

        const [userData, guilds] = await axios.all([
          axios.get('https://discordapp.com/api/users/@me', sharedConfig),
          axios.get(
            'https://discordapp.com/api/users/@me/guilds',
            sharedConfig,
          ),
        ]);

        const userAllowed = guilds.data.reduce((prev, current) => {
          return (
            prev || process.env.DISCORD_ALLOWED_GUILDS.includes(current.id)
          );
        }, false);

        // All checks are boiled down into one if
        if (userAllowed) {
          return userData.data;
        } else {
          return new ApolloError('User not in any allowed guilds', 403);
        }
      } catch (e) {
        console.error(e);
        return new ApolloError('Login failed');
      }
    },
  },
  Mutation: {
    /**
     *
     * @param {unknown} _ Parent query access, should not be needed here
     * @param {{ token: string }} data user object with discord token attached
     * @returns {{ email: string }}
     */
    register_user: async (_, data) => {
      console.log('re-implement');
      return {};
    },
  },
};
