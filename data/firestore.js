import { Firestore } from '@google-cloud/firestore';

/**
 * @type {Firestore | null}
 */
let db = null;

/**
 * Mostly GCP credentials, but if you need anything custom, initialize it here
 * @param {object} config The full config object
 * @param {string} config.projectId GCP project ID
 * @param {string} config.client_email GCP email to use for firestore
 * @param {string} config.private_key GCP private key to use for firestore
 * @returns
 */
export const initDB = () => {
  // If db already exists in memory, don't waste memory with a new client
  db =
    db ||
    new Firestore({
      projectId: process.env.GCP_PROJECT_ID,
      credentials: {
        client_email: process.env.GCP_CLIENT_EMAIL,
        private_key: process.env.GCP_PRIVATE_KEY,
      },
    });
  return db;
};
