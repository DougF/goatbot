export const discordAvatar = (id: string, avatar: string) =>
  `https://cdn.discordapp.com/avatars/${id}/${avatar}.png`;
