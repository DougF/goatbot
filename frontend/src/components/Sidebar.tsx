import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { useUserState } from '../context/userContext';

import GoatIcon from '../assets/goat.png';
import './Sidebar.css';

type SidebarProps = RouteComponentProps;

const routes = [
  {
    path: '/shopping',
    name: 'Shopping',
  },
  {
    path: '/smart-home',
    name: 'Smart Home',
  },
  {
    path: '/shared-notes',
    name: 'Shared Notes',
  },
];

const Sidebar: React.FC<SidebarProps> = (props) => {
  const { user, setUser } = useUserState();

  const currentRoute = props.location.pathname;
  const routeMatches = (routes: string[]) => {
    return routes.includes(currentRoute);
  };

  return (
    <aside className="sidebar expanded">
      <div className="sidebar-icon-wrapper">
        <img className="sidebar-icon" src={GoatIcon} alt="GoatBot" />
      </div>
      <div className="sidebar-title title-role">
        <Link to="/">Goat Management</Link>
      </div>
      <div className="sidebar-auth-section">
        {user ? (
          <>
            <div className="sidebar-logout">
              <div className="sidebar-profile">
                <span className="sidebar-profile-img">
                  <img
                    src={`https://cdn.discordapp.com/avatars/${user?.discord?.id}/${user?.discord?.avatar}.png`}
                  />
                </span>
                {user.discord?.username}
              </div>
              <button
                className="button button-secondary sidebar-logout-btn"
                onClick={() => setUser(null)}
              >
                Logout
              </button>
            </div>
          </>
        ) : (
          <>
            <Link
              to="/login"
              className={`button button-primary sidebar-login ${
                routeMatches(['/login']) && 'active'
              }`}
            >
              Login
            </Link>
            <Link
              to="/register"
              className={`button button-primary sidebar-register ${
                routeMatches(['/register', '/register-callback']) && 'active'
              }`}
            >
              Register
            </Link>
          </>
        )}
      </div>
      <div className="route-container">
        <div className="route-title">
          <h3>Page Directory</h3>
        </div>
        {routes.map((route) => {
          return (
            <div
              key={`sidebar-route-${route.path}`}
              className={`route-item ${routeMatches([route.path]) && 'active'}`}
            >
              <Link to={route.path}>{route.name}</Link>
            </div>
          );
        })}
      </div>
    </aside>
  );
};

export default withRouter(Sidebar);
