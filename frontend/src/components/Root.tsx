import { gql, useQuery } from '@apollo/client';
import React from 'react';
import type { Query } from '../generated/graphql';

const HELLO_QUERY = gql`
  query HelloQuery {
    hello_world
  }
`;

const RootPage: React.FC = () => {
  const { data, loading, error } = useQuery<Query>(HELLO_QUERY);
  if (loading) {
    return <div>Loading query...</div>;
  }
  if (error) {
    return <div>{JSON.stringify(error)}</div>;
  }
  return <div>Message: {data?.hello_world}</div>;
};

export default RootPage;
