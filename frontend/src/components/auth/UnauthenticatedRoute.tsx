import React from 'react';
import { Redirect } from 'react-router';
import { useUserState } from '../../context/userContext';

export const UnauthenticatedRoute: React.FC = ({ children }) => {
  const { user } = useUserState();
  return user ? <Redirect to="/" /> : <>{children}</>;
};
