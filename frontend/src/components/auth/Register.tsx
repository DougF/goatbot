import { gql, useMutation, useQuery } from '@apollo/client';
import React, { useEffect, useState } from 'react';
import type { RouteComponentProps } from 'react-router';

import type { Mutation, Query, User } from 'src/generated/graphql';
import { discordAvatar } from '../../utils/formatImages';
import { useUserState } from '../../context/userContext';

import './Register.css';
import { UnauthenticatedRoute } from './UnauthenticatedRoute';

export const Register: React.FC = () => {
  const oauth_url: string = import.meta.env.SNOWPACK_PUBLIC_DISCORD_URL;
  return (
    <UnauthenticatedRoute>
      <article className="register-page">
        <h2>Register for Goat Management</h2>
        <p className="register-page-disclaimer">
          Goat Management currently only supports logging in with Discord, and
          even then, for select accounts. If you aren't actively involved in any
          servers GoatBot is linked to, please use a demo account{' '}
          <b>(demo accounts WIP)</b>, and consider forking the main repository{' '}
          <a href="https://gitlab.com/DougF/goatbot">here</a>.
        </p>
        <a className="button button-discord" href={oauth_url}>
          Connect Discord Account
        </a>
      </article>
    </UnauthenticatedRoute>
  );
};

const GET_DISCORD_INFO_QUERY = gql`
  query GetDiscordInfo($token: String!) {
    getDiscordInformation(token: $token) {
      id
      username
      avatar
      discriminator
      public_flags
      flags
      locale
      mfa_enabled
      premium_type
      email
      verified
    }
  }
`;

const REGISTER_USER_MUTATION = gql`
  mutation RegisterUser($token: String!) {
    register_user(token: $token) {
      id
      username
      avatar
      discriminator
      public_flags
      flags
      locale
      mfa_enabled
      premium_type
      verified

      token
    }
  }
`;

type RegisterCallbackProps = RouteComponentProps;
export const RegisterCallback: React.FC<RegisterCallbackProps> = (props) => {
  const params = props.location.search;
  const { user, setUser } = useUserState();
  const [state, setState] = useState({
    password: '',
    confirmPassword: '',
  });

  // Fetch user's discord data with received code
  const { data, error, loading } = useQuery<Query>(GET_DISCORD_INFO_QUERY, {
    variables: {
      token: props.location.search.split('?code=')[1],
    },
  });
  const [registerUser] = useMutation<Mutation>(REGISTER_USER_MUTATION);

  useEffect(() => {
    // const callMutation = async (code: string) => {
    //   const response = await registerUser({
    //     variables: {
    //       token: code,
    //     },
    //   });
    //   console.log(response);
    //   if (response.data !== null && response.data !== undefined) {
    //     if (!response.data.register_user.token) {
    //       console.error('EXPECTED TOKEN BUT GOT UNDEFINED');
    //     }
    //     setUser(response.data.register_user);
    //   }
    // };
    // if (!user && params.length > 0 && params.includes('?code')) {
    //   callMutation(props.location.search.split('?code=')[1]);
    // }
  }, [user, setUser, registerUser, params]);

  return (
    <UnauthenticatedRoute>
      <article className="register-page">
        {loading && <h2>Loading...</h2>}
        {data && (
          <>
            <div className="register-user-confirmation">
              <div className="register-user-profile">
                <img
                  src={discordAvatar(
                    data.getDiscordInformation.id,
                    data.getDiscordInformation.avatar,
                  )}
                  alt={data.getDiscordInformation.username}
                />
                <div className="register-user-intro">
                  <h2>Welcome, {data.getDiscordInformation.username}!</h2>
                  <p>
                    There's just one more step, setting your password. You'll
                    use this as well as your email next time you log in, so{' '}
                    <i>keep it safe</i>.
                  </p>
                </div>
              </div>
              <div className="register-user-form">
                <h2>Set a password and register your account</h2>
                <div className="informational-inputs">
                  <div>
                    <span className="informational-label">Discord Email</span>
                    <span>{data.getDiscordInformation.email}</span>
                  </div>
                  <div>
                    <span className="informational-label">
                      Discord Username
                    </span>
                    <span>{data.getDiscordInformation.username}</span>
                  </div>
                </div>
                <div className="register-user-inputs">
                  <input
                    onInput={(e) =>
                      setState({ ...state, password: e.currentTarget.value })
                    }
                    value={state.password}
                    type="password"
                    name="password"
                    placeholder="Password"
                  />
                  <input
                    onInput={(e) =>
                      setState({
                        ...state,
                        confirmPassword: e.currentTarget.value,
                      })
                    }
                    value={state.confirmPassword}
                    type="password"
                    name="password-match"
                    placeholder="Confirm Password"
                  />
                </div>
                <div className="form-validation">
                  {state.password !== state.confirmPassword &&
                    state.confirmPassword !== '' && (
                      <p>Passwords don't match</p>
                    )}
                </div>
                <div className="form-controls">
                  <button className="button button-primary">Save</button>
                </div>
              </div>
              <pre>{JSON.stringify(data.getDiscordInformation, null, 2)}</pre>
            </div>
          </>
        )}
        {error && <div>{JSON.stringify(error)}</div>}
      </article>
    </UnauthenticatedRoute>
  );
};
