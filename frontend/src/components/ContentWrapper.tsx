import React from 'react';

import './ContentWrapper.css';

export const ContentWrapper: React.FC = ({ children }) => {
  return <main className="content">{children}</main>;
};

export default ContentWrapper;
