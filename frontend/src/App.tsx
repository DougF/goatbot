import React from 'react';
import Sidebar from './components/Sidebar';
import { ContentWrapper } from './components/ContentWrapper';

import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import RootPage from './components/Root';
import { Register, RegisterCallback } from './components/auth/Register';
import { ApolloContext } from './hoc/apollo';
import { UserContextProvider } from './context/userContext';

interface AppProps {}

function App({}: AppProps) {
  return (
    <div className="app-wrapper">
      <ApolloContext>
        <UserContextProvider>
          <BrowserRouter>
            {/* wrapper required to force react router to see router context */}
            <Route path="/">
              <Sidebar />
            </Route>
            <ContentWrapper>
              <Switch>
                <Route path="/" exact component={RootPage} />
                <Route path="/register" exact component={Register} />
                <Route path="/register-callback" component={RegisterCallback} />
                <Route
                  path="*"
                  exact
                  component={() => {
                    return <div>404: Page not found</div>;
                  }}
                />
              </Switch>
            </ContentWrapper>
          </BrowserRouter>
        </UserContextProvider>
      </ApolloContext>
    </div>
  );
}

export default App;
