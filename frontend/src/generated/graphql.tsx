import { gql } from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};


export type Query = {
  __typename?: 'Query';
  hello_world: Scalars['String'];
  getDiscordInformation: DiscordUser;
};


export type QueryGetDiscordInformationArgs = {
  token: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  register_user: User;
  login: User;
};


export type MutationRegister_UserArgs = {
  token: Scalars['String'];
};


export type MutationLoginArgs = {
  token: Scalars['String'];
};

export type DiscordUser = {
  __typename?: 'DiscordUser';
  id: Scalars['String'];
  username: Scalars['String'];
  avatar: Scalars['String'];
  discriminator: Scalars['String'];
  public_flags: Scalars['Int'];
  flags: Scalars['Int'];
  locale: Scalars['String'];
  mfa_enabled: Scalars['Boolean'];
  premium_type?: Maybe<Scalars['Int']>;
  email: Scalars['String'];
  verified: Scalars['Boolean'];
};

export type DiscordUserInput = {
  id: Scalars['String'];
  username: Scalars['String'];
  avatar: Scalars['String'];
  discriminator: Scalars['String'];
  public_flags: Scalars['Int'];
  flags: Scalars['Int'];
  locale: Scalars['String'];
  mfa_enabled: Scalars['Boolean'];
  premium_type?: Maybe<Scalars['Int']>;
  email: Scalars['String'];
  verified: Scalars['Boolean'];
};

export type UserRegistrationInput = {
  discord?: Maybe<DiscordUserInput>;
  password: Scalars['String'];
};

export type User = {
  __typename?: 'User';
  id: Scalars['String'];
  password?: Maybe<Scalars['String']>;
  discord?: Maybe<DiscordUser>;
  token?: Maybe<Scalars['String']>;
};

export enum CacheControlScope {
  Public = 'PUBLIC',
  Private = 'PRIVATE'
}

