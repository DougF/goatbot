import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import React from 'react';

const client = new ApolloClient({
  uri: import.meta.env.SNOWPACK_PUBLIC_GRAPHQL_URL,
  cache: new InMemoryCache(),
});

export const ApolloContext: React.FC = ({ children }) => {
  return <ApolloProvider client={client}>{children}</ApolloProvider>;
};
