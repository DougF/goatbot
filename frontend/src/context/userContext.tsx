import React from 'react';
import type { User } from '../generated/graphql';

type UserState = User | null;

// Technically this is undefined, but we throw an error below if you try to use it when undefined
const UserContext = React.createContext<
  { user: UserState; setUser: React.Dispatch<UserState> } | undefined
>(undefined);

export const useUserState = () => {
  const context = React.useContext(UserContext);
  if (!context) {
    throw new Error('Cannot use User context outside of UserContextProvider');
  }
  return context;
};

export const UserContextProvider: React.FC = (props) => {
  const [user, setUser] = React.useState<UserState>(null);

  return <UserContext.Provider value={{ user, setUser }} {...props} />;
};
