// ==========================================
// Imports
// ==========================================
import express from 'express';
import { config as configDotenv } from 'dotenv';

import { useGoatBot } from './bot/core.js';
import { Logger } from './logger/log.js';
import { ApolloServer } from 'apollo-server-express';
import { schema, resolvers } from './server/graphql.js';

// ==========================================
// Init high level stuff
// ==========================================
configDotenv();
const log = new Logger('main.js', 'life-cycle');
const app = express();

// Graphql integration
const server = new ApolloServer({
  typeDefs: schema,
  resolvers,
  playground: process.env.NODE_ENV === 'development',
});

server.applyMiddleware({ app, path: '/graphql' });

// Setup routes
app.get('/health', (req, res) => {
  res.sendStatus(200);
});

app.get('/status', (req, res) => {
  res.send('Add more detailed status here');
});

app.get('/', (req, res) => {
  res.send('Goat bot management server!');
});

// ==========================================
// Start server process
// ==========================================
log.info('Starting bot management server');

app.listen(8000, async () => {
  log.info('Management server listening on 3000');
  log.info('Starting bot process');

  const { bot } = await useGoatBot();

  // Start bot here
  if (!bot.initialized) {
    log.error('! could not start bot. login failed !');
  }
});
